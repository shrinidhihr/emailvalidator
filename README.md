# README #

This app validates emails. 

## Instructions:

App: Just type in the email address into the textbox and press on the validate button.

To run the android app, start an emulator and run:
```
npx react-native run-android
```

## Details:
The app uses React-Native. 

Email validation is performed using [email-validator.](https://github.com/manishsaraan/email-validator)

