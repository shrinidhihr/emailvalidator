/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useState } from 'react';
import * as EmailValidator from 'email-validator';
import { View, Button, TextInput, Alert, Text } from 'react-native';

const App: () => React$Node = () => {
  const [text, setText] = useState('');
  const isValidEmail = (text) => {
    if (EmailValidator.validate(text)) {
      return text + " is a valid email address."
    }
    return text + " is not a valid email address!";
  };
  return (
    <>
      <Text style={{ fontSize: 30, backgroundColor: 'black', color: 'white', paddingLeft: 15 }}>React Native Training</Text>
      <View style={{ padding: 10, justifyContent: 'center', flex: 1, alignItems: 'center' }}>
        <TextInput
          style={{ height: 40, width: 240, borderColor: 'gray', borderWidth: 1, marginBottom: 8 }}
          placeholder="Email"
          onChangeText={text => setText(text)}
          defaultValue={text}
        />
        <Button
          title="Validate"
          color="blue"
          onPress={() => Alert.alert("Validation Result", isValidEmail(text))}
        />
      </View>
    </>
  );
};

export default App;
